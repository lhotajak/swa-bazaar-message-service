## SWA Bazaar MessageService

### How to run
- Git automatically converts LF to CRLF, the script *fix-line-endings.sh* converts
 the line endings in *db/01-init.sh* file, which is needed for proper
 PostgreSQL initialization
```
git clone git@gitlab.fel.cvut.cz:lhotajak/swa-bazaar-message-service.git
cd swa-bazaar-message-service
sh fix-line-endings.sh
docker-compose up -d
```
##### How to stop running container
```
docker-compose down
```
##### How to stop running container & delete database content
```
docker-compose down --volumes
```

### Swagger documentation
http://localhost:8083/swagger-ui/index.html

### Ports
- :5454 - Postgres DB
- :8083 - MessageService

### Docker Hub
https://hub.docker.com/r/lhotajak98/fel-swa-message-service
