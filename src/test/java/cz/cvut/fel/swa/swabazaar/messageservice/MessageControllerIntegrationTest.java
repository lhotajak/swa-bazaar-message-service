package cz.cvut.fel.swa.swabazaar.messageservice;

import cz.cvut.fel.swa.swabazaar.messageservice.message.Message;
import cz.cvut.fel.swa.swabazaar.messageservice.message.MessageController;
import cz.cvut.fel.swa.swabazaar.messageservice.message.MessageRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class MessageControllerIntegrationTest {
    @Autowired
    private MessageRepository repository;

    @Autowired
    private MessageController controller;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
    }

    private String getBaseURL() {
        return "http://localhost:" + port + "/api/v1/messages";
    }

    @Test
    public void emptyGetMessagesTest() {
        repository.deleteAll();

        String respString = this.restTemplate.getForObject(getBaseURL(), String.class);

        assertThat(respString).isEqualTo("[]");
    }

    @Test
    public void nonEmptyGetMessagesTest() throws JSONException {
        repository.deleteAll();
        Message msg = new Message(4L, 5L, "TITLE", "TEXT", System.currentTimeMillis());
        repository.save(msg);

        String respString = this.restTemplate.getForObject(getBaseURL(), String.class);

        JSONArray array = new JSONArray(respString);
        assertThat(array.length()).isEqualTo(1);
    }

    @Test
    public void getMessagesFromUserIdTest() throws JSONException {
        repository.deleteAll();
        long fromId = 4, toId = 5;
        Message msg;
        msg = new Message(fromId, toId, "TARGET", "TEXT", System.currentTimeMillis());
        repository.save(msg);
        msg = new Message(toId, fromId, "TITLE", "TEXT", System.currentTimeMillis());
        repository.save(msg);

        String respString = this.restTemplate.getForObject(getBaseURL() + "/from/" + fromId, String.class);

        JSONArray array = new JSONArray(respString);
        assertThat(array.length()).isEqualTo(1);
        assertThat(array.getJSONObject(0).get("title")).isEqualTo("TARGET");
    }

    @Test
    public void getMessagesToUserIdTest() throws JSONException {
        repository.deleteAll();
        long fromId = 4, toId = 5;
        Message msg;
        msg = new Message(fromId, toId, "TARGET", "TEXT", System.currentTimeMillis());
        repository.save(msg);
        msg = new Message(toId, fromId, "TITLE", "TEXT", System.currentTimeMillis());
        repository.save(msg);

        String respString = this.restTemplate.getForObject(getBaseURL() + "/to/" + toId, String.class);

        JSONArray array = new JSONArray(respString);
        assertThat(array.length()).isEqualTo(1);
        assertThat(array.getJSONObject(0).get("title")).isEqualTo("TARGET");
    }


    @Test
    public void getMessagesFromUserIdToUserIdTest() throws JSONException {
        repository.deleteAll();
        long fromId = 4, toId = 5;
        Message msg;
        msg = new Message(fromId, toId, "TARGET", "TEXT", System.currentTimeMillis());
        repository.save(msg);
        msg = new Message(toId, fromId, "TITLE", "TEXT", System.currentTimeMillis());
        repository.save(msg);

        String respString = this.restTemplate.getForObject(
                getBaseURL() + "/from/" + fromId + "/to/" + toId, String.class);

        JSONArray array = new JSONArray(respString);
        assertThat(array.length()).isEqualTo(1);
        assertThat(array.getJSONObject(0).get("title")).isEqualTo("TARGET");
    }

    @Test
    public void sendMessageTest() {
        repository.deleteAll();
        long fromId = 4, toId = 5;
        Message msg;
        msg = new Message(fromId, toId, "TARGET", "TEXT");

        this.restTemplate.postForObject(getBaseURL(), msg, Message.class);

        List<Message> all = repository.findAll();
        assertThat(all.size()).isEqualTo(1);
        assertThat(all.get(0).getTitle()).isEqualTo("TARGET");
    }

    @Test
    public void timestampFromQueryTest() throws JSONException {
        repository.deleteAll();
        long timestamp = System.currentTimeMillis();
        int N = 5;
        for (int i = 0; i < N; i++) {
            repository.save(new Message(1L, 2L, "TITLE", "TEXT", timestamp - 1 - i));
        }
        for (int i = 0; i < N; i++) {
            repository.save(new Message(1L, 2L, "TARGET", "TEXT", timestamp + i));
        }

        String respString = this.restTemplate.getForObject(
                getBaseURL() + "?timestamp_from=" + timestamp, String.class);

        JSONArray array = new JSONArray(respString);
        assertThat(array.length()).isEqualTo(N);
        int returnedTargetN = 0;
        for (int i = 0; i < array.length(); i++) {
            String t = array.getJSONObject(i).getString("title");
            if ("TARGET".equals(t)) ++returnedTargetN;
        }
        assertThat(returnedTargetN).isEqualTo(N);
    }

    @Test
    public void timestampToQueryTest() throws JSONException {
        repository.deleteAll();
        long timestamp = System.currentTimeMillis();
        int N = 5;
        for (int i = 0; i < N; i++) {
            repository.save(new Message(1L, 2L, "TARGET", "TEXT", timestamp - i));
        }
        for (int i = 0; i < N; i++) {
            repository.save(new Message(1L, 2L, "TITLE", "TEXT", timestamp + 1 + i));
        }

        String respString = this.restTemplate.getForObject(
                getBaseURL() + "?timestamp_to=" + timestamp, String.class);

        JSONArray array = new JSONArray(respString);
        assertThat(array.length()).isEqualTo(N);
        int returnedTargetN = 0;
        for (int i = 0; i < array.length(); i++) {
            String t = array.getJSONObject(i).getString("title");
            if ("TARGET".equals(t)) ++returnedTargetN;
        }
        assertThat(returnedTargetN).isEqualTo(N);
    }

    @Test
    public void timestampFromToQueryTest() throws JSONException {
        repository.deleteAll();
        long timestamp = System.currentTimeMillis();
        int N = 5;
        for (int i = 0; i < N; i++) {
            repository.save(new Message(1L, 2L, "TITLE", "TEXT", timestamp - 1 - i));
        }
        for (int i = 0; i < N; i++) {
            repository.save(new Message(1L, 2L, "TARGET", "TEXT", timestamp + i));
        }
        for (int i = 0; i < N; i++) {
            repository.save(new Message(1L, 2L, "TITLE", "TEXT", timestamp + 1 + N + i));
        }


        String respString = this.restTemplate.getForObject(
                getBaseURL() + "?timestamp_from=" + timestamp + "&timestamp_to=" + (timestamp + N), String.class);

        JSONArray array = new JSONArray(respString);
        assertThat(array.length()).isEqualTo(N);
        int returnedTargetN = 0;
        for (int i = 0; i < array.length(); i++) {
            String t = array.getJSONObject(i).getString("title");
            if ("TARGET".equals(t)) ++returnedTargetN;
        }
        assertThat(returnedTargetN).isEqualTo(N);
    }
}
