package cz.cvut.fel.swa.swabazaar.messageservice;

import cz.cvut.fel.swa.swabazaar.messageservice.message.TimestampInfo;
import org.junit.Test;
import org.springframework.web.server.ResponseStatusException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TimestampInfoTest {
    @Test
    public void emptyTimestampInfoTest() {
        TimestampInfo info = new TimestampInfo(null, null);

        assertThat(info.getTimestampFrom()).isEqualTo(0L);
        assertThat(info.getTimestampTo() > info.getTimestampFrom()).isEqualTo(true);
    }

    @Test
    public void emptyToTimestampInfoTest() {
        long value = 123456L;

        TimestampInfo info = new TimestampInfo(value, null);

        assertThat(info.getTimestampFrom()).isEqualTo(value);
        assertThat(info.getTimestampTo() > info.getTimestampFrom()).isEqualTo(true);
    }

    @Test
    public void emptyFromTimestampInfoTest() {
        long value = 123456L;

        TimestampInfo info = new TimestampInfo(null, value);

        assertThat(info.getTimestampTo()).isEqualTo(value);
        assertThat(info.getTimestampTo() > info.getTimestampFrom()).isEqualTo(true);
    }


    @Test
    public void nonEmptyTimestampInfoTest() {
        long from = 123456L;
        long to = from * 2 + 1;

        TimestampInfo info = new TimestampInfo(from, to);

        assertThat(info.getTimestampFrom()).isEqualTo(from);
        assertThat(info.getTimestampTo()).isEqualTo(to);
    }


    @Test
    public void InvalidTimestampInfoTest() {
        long from = -1;

        String exMsg = null;
        try {
            TimestampInfo info = new TimestampInfo(from, null);
        } catch (ResponseStatusException exception) {
            exMsg = exception.getMessage();
        }

        assertThat(exMsg).isNotNull();
        assertThat(exMsg).contains("Timestamp error: timestampFrom has to be null or >= 0");
    }
}
