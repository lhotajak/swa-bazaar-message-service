package cz.cvut.fel.swa.swabazaar.messageservice;

import cz.cvut.fel.swa.swabazaar.messageservice.message.*;
import cz.cvut.fel.swa.swabazaar.messageservice.utils.Utils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ValidatorTest {

    @Autowired
    private MessageRepository repository;

    @Autowired
    private Validator validator;

    @LocalServerPort
    private int port;

    @Test
    public void contextLoads() {
        assertThat(validator).isNotNull();
    }

    private Message createTestMsg() {
        return new Message(1L, 1L, 2L, "TITLE", "TEXT", System.currentTimeMillis());
    }

    @Test
    public void messageOkTest() {
        validator.validateMessage(createTestMsg());
    }

    @Test
    public void fromUserIdMissingValidatorTest() {
        Message message = createTestMsg();
        message.setFromUserId(null);

        String msg = null;
        try {
            validator.validateMessage(message);
        } catch (ResponseStatusException exception) {
            msg = exception.getMessage();
        }
        assertThat(msg).isNotNull();
        assertThat(msg).contains("Missing property: fromUserId");
    }

    @Test
    public void toUserIdMissingValidatorTest() {
        Message message = createTestMsg();
        message.setToUserId(null);

        String msg = null;
        try {
            validator.validateMessage(message);
        } catch (ResponseStatusException exception) {
            msg = exception.getMessage();
        }
        assertThat(msg).isNotNull();
        assertThat(msg).contains("Missing property: toUserId");
    }


    @Test
    public void titleMissingValidatorTest() {
        Message message = createTestMsg();
        message.setTitle(null);

        String msg = null;
        try {
            validator.validateMessage(message);
        } catch (ResponseStatusException exception) {
            msg = exception.getMessage();
        }
        assertThat(msg).isNotNull();
        assertThat(msg).contains("Missing property: title");
    }


    @Test
    public void textMissingValidatorTest() {
        Message message = createTestMsg();
        message.setText(null);

        String msg = null;
        try {
            validator.validateMessage(message);
        } catch (ResponseStatusException exception) {
            msg = exception.getMessage();
        }
        assertThat(msg).isNotNull();
        assertThat(msg).contains("Missing property: text");
    }

    @Test
    public void invalidTitleSizeValidatorTest() {
        Message message = createTestMsg();
        message.setTitle(Utils.randomString(validator.getMAX_TITLE_SIZE() + 1));

        String msg = null;
        try {
            validator.validateMessage(message);
        } catch (ResponseStatusException exception) {
            msg = exception.getMessage();
        }
        assertThat(msg).isNotNull();
        assertThat(msg).contains("Invalid title size");
    }

    @Test
    public void invalidTextSizeValidatorTest() {
        Message message = createTestMsg();
        message.setText(Utils.randomString(validator.getMAX_TEXT_SIZE() + 1));

        String msg = null;
        try {
            validator.validateMessage(message);
        } catch (ResponseStatusException exception) {
            msg = exception.getMessage();
        }
        assertThat(msg).isNotNull();
        assertThat(msg).contains("Invalid text size");
    }
}
