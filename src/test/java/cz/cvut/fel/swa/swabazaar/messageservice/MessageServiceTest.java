package cz.cvut.fel.swa.swabazaar.messageservice;

import cz.cvut.fel.swa.swabazaar.messageservice.message.Message;
import cz.cvut.fel.swa.swabazaar.messageservice.message.MessageRepository;
import cz.cvut.fel.swa.swabazaar.messageservice.message.MessageService;
import cz.cvut.fel.swa.swabazaar.messageservice.message.TimestampInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class MessageServiceTest {
    // default timestamp info
    private static final TimestampInfo DEFAULT_TIMESTAMP_INFO = new TimestampInfo(null, null);

    @Autowired
    private MessageRepository repository;

    @Autowired
    private MessageService service;

    @LocalServerPort
    private int port;

    @Test
    public void contextLoads() {
        assertThat(service).isNotNull();
    }

    @Test
    public void emptyGetMessagesTest() {
        repository.deleteAll();

        List<Message> msgList = service.getMessages(DEFAULT_TIMESTAMP_INFO);

        assertThat(msgList.size()).isEqualTo(0);
    }

    @Test
    public void nonEmptyGetMessagesTest() {
        repository.deleteAll();
        repository.save(new Message(1L, 2L, "TITLE", "TEXT", System.currentTimeMillis()));

        List<Message> msgList = service.getMessages(DEFAULT_TIMESTAMP_INFO);

        assertThat(msgList.size()).isEqualTo(1);
    }


    private long clearAndCreateTestMessage() {
        repository.deleteAll();
        Message res = repository.save(new Message(1L, 2L, "TITLE", "TEXT", System.currentTimeMillis()));
        return res.getId();
    }

    @Test
    public void getMessageTest() {
        long msgId = clearAndCreateTestMessage();

        Message message = service.getMessage(msgId);

        assertThat(message.getTitle()).isEqualTo("TITLE");
    }

    @Test
    public void failedGetMessageTest() {
        long msgId = clearAndCreateTestMessage();
        long findMsgId = msgId + 1;
        String msg = null;
        try {
            Message message = service.getMessage(findMsgId);
        } catch (ResponseStatusException exception) {
            msg = exception.getMessage();
        }
        assertThat(msg).isNotNull();
        assertThat(msg).contains("Get message error - message with id " + findMsgId + " not found");
    }

    @Test
    public void deleteMessageTest() {
        long msgId = clearAndCreateTestMessage();
        service.deleteMessage(msgId);
    }

    @Test
    public void failedDeleteMessageTest() {
        long msgId = clearAndCreateTestMessage();
        long findMsgId = msgId + 1;

        String msg = null;
        try {
            service.deleteMessage(findMsgId);
        } catch (ResponseStatusException exception) {
            msg = exception.getMessage();
        }
        assertThat(msg).isNotNull();
        assertThat(msg).contains("Delete message error - message with id " + findMsgId + " not found");
    }

    @Test
    public void getMessagesFromUserIdTest() {
        long msgId = clearAndCreateTestMessage();

        List<Message> msgList = service.getMessagesFromUserId(1L, DEFAULT_TIMESTAMP_INFO);

        assertThat(msgList.size()).isEqualTo(1);
        assertThat(msgList.get(0).getId()).isEqualTo(msgId);
    }

    @Test
    public void getMessagesToUserIdTest() {
        long msgId = clearAndCreateTestMessage();

        List<Message> msgList = service.getMessagesToUserId(2L, DEFAULT_TIMESTAMP_INFO);

        assertThat(msgList.size()).isEqualTo(1);
        assertThat(msgList.get(0).getId()).isEqualTo(msgId);
    }

    @Test
    public void getMessagesFromUserIdToUserIdTest() {
        long msgId = clearAndCreateTestMessage();

        List<Message> msgList = service.getMessagesFromUserIdToUserId(1L, 2L, DEFAULT_TIMESTAMP_INFO);

        assertThat(msgList.size()).isEqualTo(1);
        assertThat(msgList.get(0).getId()).isEqualTo(msgId);
    }

    @Test
    public void sendMessagesTest() {
        repository.deleteAll();

        Message msg = service.sendMessages(new Message(10L, 20L, "TITLE", "TEXT"));

        Optional<Message> msgOpt = repository.findById(msg.getId());
        assertThat(msgOpt.isPresent()).isEqualTo(true);
        assertThat(msgOpt.get().getId()).isEqualTo(msg.getId());
    }

    @Test
    public void getMessagesUserIdTest() {
        Long userId = 1L;
        repository.deleteAll();
        Message mes1 = repository.save(new Message(userId, 2L, "TITLE", "TEXT", System.currentTimeMillis()));
        Long id1 = mes1.getId();
        Message mes2 = repository.save(new Message(3L, userId, "TITLE", "TEXT", System.currentTimeMillis()));
        Long id2 = mes2.getId();
        List<Message> msgList = service.getMessagesUserId(userId, DEFAULT_TIMESTAMP_INFO);

        Set<Long> idList = msgList.stream().map(Message::getId).collect(Collectors.toSet());
        assertThat(idList.size()).isEqualTo(2);
        assertThat(idList.contains(id1)).isTrue();
        assertThat(idList.contains(id2)).isTrue();
    }
}
