package cz.cvut.fel.swa.swabazaar.messageservice.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * Class for business logic
 */
@Service
public class MessageService {

    private static final Logger log = LoggerFactory.getLogger(MessageService.class);
    private final MessageRepository messageRepository;
    private final Validator validator;

    @Autowired
    public MessageService(MessageRepository messageRepository, Validator validator) {
        this.messageRepository = messageRepository;
        this.validator = validator;
    }

    public List<Message> getMessages(TimestampInfo timestampInfo) {
        log.info("Fetching all messages from database (timestampFrom: "
                + timestampInfo.getTimestampFrom() + ", timestampTo: " + timestampInfo.getTimestampTo() + ")");
        return messageRepository.getAllMessages(timestampInfo.getTimestampFrom(), timestampInfo.getTimestampTo());
    }

    public Message getMessage(Long id) {
        Optional<Message> optMsg = messageRepository.findById(id);
        if (optMsg.isEmpty()) {
            String m = "Get message error - message with id " + id + " not found";
            log.error(m);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, m); // HTTP 404 - NOT_FOUND
        }
        return optMsg.get();
    }

    public void deleteMessage(Long id) {
        Optional<Message> optMsg = messageRepository.findById(id);
        if (optMsg.isEmpty()) {
            String m = "Delete message error - message with id " + id + " not found";
            log.error(m);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, m); // HTTP 404 - NOT_FOUND
        }
        messageRepository.deleteById(id);
    }


    public List<Message> getMessagesFromUserId(Long fromUserId, TimestampInfo timestampInfo) {
        return messageRepository.findMessagesFromUserId(fromUserId, timestampInfo.getTimestampFrom(), timestampInfo.getTimestampTo());
    }

    public List<Message> getMessagesToUserId(Long toUserId, TimestampInfo timestampInfo) {
        return messageRepository.findMessagesToUserId(toUserId, timestampInfo.getTimestampFrom(), timestampInfo.getTimestampTo());
    }

    public List<Message> getMessagesFromUserIdToUserId(Long fromUserId, Long toUserId, TimestampInfo timestampInfo) {
        return messageRepository.findMessagesFromUserIdToUserId(fromUserId, toUserId, timestampInfo.getTimestampFrom(), timestampInfo.getTimestampTo());
    }

    public Message sendMessages(Message message) {
        validator.validateMessage(message);
        message.setTimestamp(System.currentTimeMillis());

        // message OK
        return messageRepository.save(message);
    }

    public List<Message> getMessagesUserId(Long userId, TimestampInfo timestampInfo) {
        return messageRepository.findMessagesUserId(userId, timestampInfo.getTimestampFrom(), timestampInfo.getTimestampTo());
    }
}
