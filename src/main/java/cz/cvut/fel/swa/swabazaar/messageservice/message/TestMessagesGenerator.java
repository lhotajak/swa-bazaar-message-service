package cz.cvut.fel.swa.swabazaar.messageservice.message;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

/**
 * Configuration class
 * Only generates few artificial messages
 */
@Configuration
public class TestMessagesGenerator {

    private static final boolean GENERATE_MESSAGES = false;
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();
    static final int N = 10, M = 10;

    // CommandLineRunner is a special bean that lets you execute
    // some logic after the application context is loaded and started.
    @Bean
    CommandLineRunner commandLineRunner(MessageRepository messageRepository) {
        return (String... args) -> {
            if (GENERATE_MESSAGES){
                List<Message> messages = generateTestMessages();
                messageRepository.saveAll(messages);
            }
        };
    }

    private String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    private List<Message> generateTestMessages() {
        List<Message> messages = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                messages.add(new Message((long) i, (long) (rnd.nextInt(N)), randomString(10), randomString(10), Math.abs(rnd.nextLong())));
            }
        }
        return messages;
    }
}
