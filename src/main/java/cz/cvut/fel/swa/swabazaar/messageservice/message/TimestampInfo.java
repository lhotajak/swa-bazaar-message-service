package cz.cvut.fel.swa.swabazaar.messageservice.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class TimestampInfo {
    private static final Logger log = LoggerFactory.getLogger(TimestampInfo.class);

    private final Long timestampFrom;
    private final Long timestampTo;

    public TimestampInfo(Long timestampFrom, Long timestampTo) {
        if (timestampFrom != null && timestampFrom < 0){
            String msg = "Timestamp error: timestampFrom has to be null or >= 0";
            log.error(msg);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg); // HTTP 400 - BAD_REQUEST
        }
        if (timestampTo != null && timestampTo < 0){
            String msg = "Timestamp error: timestampTo has to be null or >= 0";
            log.error(msg);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
        }
        this.timestampFrom = timestampFrom;
        this.timestampTo = timestampTo;
        if (getTimestampTo() < getTimestampFrom()) {
            String msg = "Timestamp error: timestampTo < timestampFrom";
            log.error(msg);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
        }
    }

    public Long getTimestampFrom() {
        if (timestampFrom == null) return 0L;
        return timestampFrom;
    }

    public Long getTimestampTo() {
        if (timestampTo == null) return System.currentTimeMillis();
        return timestampTo;
    }
}
