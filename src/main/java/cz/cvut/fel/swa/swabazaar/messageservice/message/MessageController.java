package cz.cvut.fel.swa.swabazaar.messageservice.message;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "Swagger2MessageRestController", tags = {"Message REST API"})
@RestController
@RequestMapping(path = "api/v1/messages")
public class MessageController {

    private static final Logger log = LoggerFactory.getLogger(MessageController.class);
    private final MessageService messageService;

    // dependency injection -> MessageService must be a bean
    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    // GET MESSAGES
    @ApiOperation(value = "Get list of all Messages in database",
            response = Message.class, responseContainer = "List", tags = "Message REST API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Server error")}
    )
    @RequestMapping(path = "", method = RequestMethod.GET, produces = "application/json")
    public List<Message> getMessages(@RequestParam(name = "timestamp_from", required = false) Long timestampFrom,
                                     @RequestParam(name = "timestamp_to", required = false) Long timestampTo) {
        log.warn("Request for obtaining all the messages from the database has been requested");
        return messageService.getMessages(new TimestampInfo(timestampFrom, timestampTo));
    }

    // GET MESSAGE
    @ApiOperation(value = "Get Message by ID",
            response = Message.class, tags = "Message REST API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Message not found"),
            @ApiResponse(code = 500, message = "Server error")}
    )
    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Message getMessage(@PathVariable("id") Long id) {
        return messageService.getMessage(id);
    }

    // DELETE MESSAGE
    @ApiOperation(value = "Delete Message by ID", tags = "Message REST API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Message not found"),
            @ApiResponse(code = 500, message = "Server error")}
    )
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void deleteMessage(@PathVariable("id") Long id) {
        messageService.deleteMessage(id);
    }

    // GET MESSAGES FROM
    @ApiOperation(value = "Get list of all Messages in database sent from user with given id",
            response = Message.class, responseContainer = "List", tags = "Message REST API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Server error")}
    )
    @RequestMapping(path = "from/{fromUserId}", method = RequestMethod.GET, produces = "application/json")
    public List<Message> getMessagesFromUserId(@PathVariable("fromUserId") Long fromUserId,
                                               @RequestParam(name = "timestamp_from", required = false) Long timestampFrom,
                                               @RequestParam(name = "timestamp_to", required = false) Long timestampTo) {
        return messageService.getMessagesFromUserId(fromUserId, new TimestampInfo(timestampFrom, timestampTo));
    }

    // GET MESSAGES TO
    @ApiOperation(value = "Get list of all Messages in database sent to user with given id",
            response = Message.class, responseContainer = "List", tags = "Message REST API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Server error")}
    )
    @RequestMapping(path = "to/{toUserId}", method = RequestMethod.GET, produces = "application/json")
    public List<Message> getMessagesToUserId(@PathVariable("toUserId") Long toUserId,
                                             @RequestParam(name = "timestamp_from", required = false) Long timestampFrom,
                                             @RequestParam(name = "timestamp_to", required = false) Long timestampTo) {
        return messageService.getMessagesToUserId(toUserId, new TimestampInfo(timestampFrom, timestampTo));
    }

    // GET MESSAGES FROM TO
    @ApiOperation(value = "Get list of all Messages in database sent from user1 to user2 with given ids",
            response = Message.class, responseContainer = "List", tags = "Message REST API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Server error")}
    )
    @RequestMapping(path = "from/{fromUserId}/to/{toUserId}", method = RequestMethod.GET, produces = "application/json")
    public List<Message> getMessagesFromUserIdToUserId(@PathVariable("fromUserId") Long fromUserId,
                                                       @PathVariable("toUserId") Long toUserId,
                                                       @RequestParam(name = "timestamp_from", required = false) Long timestampFrom,
                                                       @RequestParam(name = "timestamp_to", required = false) Long timestampTo) {
        return messageService.getMessagesFromUserIdToUserId(fromUserId, toUserId, new TimestampInfo(timestampFrom, timestampTo));
    }

    // GET USER MESSAGES
    @ApiOperation(value = "Get list of all Messages in database sent or received by user with given id",
            response = Message.class, responseContainer = "List", tags = "Message REST API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Server error")}
    )
    @RequestMapping(path = "user/{userId}", method = RequestMethod.GET, produces = "application/json")
    public List<Message> getMessagesUserId(@PathVariable("userId") Long userId,
                                           @RequestParam(name = "timestamp_from", required = false) Long timestampFrom,
                                           @RequestParam(name = "timestamp_to", required = false) Long timestampTo) {
        return messageService.getMessagesUserId(userId, new TimestampInfo(timestampFrom, timestampTo));
    }

    // SEND MESSAGE
    @ApiOperation(value = "Stores new Message in database", response = Message.class, tags = "Message REST API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server error")}
    )
    @RequestMapping(path = "", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public Message sendMessage(@RequestBody Message message) {
        return messageService.sendMessages(message);
    }
}
