package cz.cvut.fel.swa.swabazaar.messageservice.utils;

import java.security.SecureRandom;
import java.time.Instant;

public class Utils {
    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();


    public static long utcTimeNow() {
        return Instant.now().toEpochMilli();
    }

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }
}
