package cz.cvut.fel.swa.swabazaar.messageservice.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table
public class Message {
    // MAPPING TO DB
    @Id
    @SequenceGenerator(
            name = "message_sequence",
            sequenceName = "message_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "message_sequence"
    )
    @ApiModelProperty(notes = "Message id", example = "3", position = 0)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;
    @ApiModelProperty(notes = "User id of sender", example = "4", position = 1)
    private Long fromUserId;
    @ApiModelProperty(notes = "User id of receiver", example = "8", position = 2)
    private Long toUserId;
    @ApiModelProperty(notes = "Title of a message", example = "Message title", position = 3)
    private String title;
    @ApiModelProperty(notes = "Text of a message", example = "Message text", position = 4)
    private String text;
    @ApiModelProperty(notes = "UTC timestamp", example = "1648132487258", position = 5, access = "WRITE_ONLY")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long timestamp;

    public Message() {
    }

    public Message(Long id, Long fromUserId, Long toUserId, String title, String text, Long timestamp) {
        this.id = id;
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.title = title;
        this.text = text;
        this.timestamp = timestamp;
    }

    // without id
    public Message(Long fromUserId, Long toUserId, String title, String text, Long timestamp) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.title = title;
        this.text = text;
        this.timestamp = timestamp;
    }

    // without id & timestamp
    public Message(Long fromUserId, Long toUserId, String title, String text) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.title = title;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", fromUserId=" + fromUserId +
                ", toUserId=" + toUserId +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
