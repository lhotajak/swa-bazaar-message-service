package cz.cvut.fel.swa.swabazaar.messageservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessageserviceApplication {

    private static final Logger log = LoggerFactory.getLogger(MessageserviceApplication.class);

    public static void main(String[] args) {
        log.info("Message service started");
        SpringApplication.run(MessageserviceApplication.class, args);

        // TODO DOCKER POSTGRES
    }
}
