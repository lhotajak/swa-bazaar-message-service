package cz.cvut.fel.swa.swabazaar.messageservice.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

/**
 * Class for validating input
 */
@Service
public class Validator {

    @Autowired
    private Environment env;

    private static final Logger log = LoggerFactory.getLogger(MessageService.class);

    public Integer getMIN_TITLE_SIZE() {
        return MIN_TITLE_SIZE;
    }

    public Integer getMAX_TITLE_SIZE() {
        return MAX_TITLE_SIZE;
    }

    public Integer getMIN_TEXT_SIZE() {
        return MIN_TEXT_SIZE;
    }

    public Integer getMAX_TEXT_SIZE() {
        return MAX_TEXT_SIZE;
    }

    // values are read from application.properties
    @Value("${messageservice.mintitlesize}")
    private Integer MIN_TITLE_SIZE;

    @Value("${messageservice.maxtitlesize}")
    private Integer MAX_TITLE_SIZE;

    @Value("${messageservice.mintextsize}")
    private Integer MIN_TEXT_SIZE;

    @Value("${messageservice.maxtextsize}")
    private Integer MAX_TEXT_SIZE;

    private boolean envRead = false;

    private void exception(String msg) {
        log.error("Validator error - " + msg);
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg); // HTTP 400 - BAD_REQUEST
    }

    public void validateMessage(Message message) {
        if (!envRead) initValues();

        Long fromUserId = message.getFromUserId();
        Long toUserId = message.getToUserId();
        String title = message.getTitle();
        String text = message.getText();

        // check not null
        if (fromUserId == null)
            exception("Missing property: fromUserId");
        if (toUserId == null)
            exception("Missing property: toUserId");
        if (title == null)
            exception("Missing property: title");
        if (text == null)
            exception("Missing property: text");

        // check string sizes
        if (title.length() > MAX_TITLE_SIZE || title.length() < MIN_TITLE_SIZE)
            exception("Invalid title size (must be between " + MIN_TITLE_SIZE + " and " + MAX_TITLE_SIZE + ")");
        if (text.length() > MAX_TEXT_SIZE || text.length() < MIN_TEXT_SIZE)
            exception("Invalid text size (must be between " + MIN_TEXT_SIZE + " and " + MAX_TEXT_SIZE + ")");
    }

    private void initValues() {
        MIN_TITLE_SIZE = env.getProperty("MIN_TITLE_SIZE", Integer.class, MIN_TITLE_SIZE);
        MAX_TITLE_SIZE = env.getProperty("MAX_TITLE_SIZE", Integer.class, MAX_TITLE_SIZE);
        MIN_TEXT_SIZE = env.getProperty("MIN_TEXT_SIZE", Integer.class, MIN_TEXT_SIZE);
        MAX_TEXT_SIZE = env.getProperty("MAX_TEXT_SIZE", Integer.class, MAX_TEXT_SIZE);
        envRead = true;
    }
}
