package cz.cvut.fel.swa.swabazaar.messageservice.message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Accessing DB
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    // JPQL syntax
    @Query("SELECT m FROM Message m WHERE m.fromUserId = ?1")
    List<Message> findMessagesFromUserId(Long fromUserId);

    @Query("SELECT m FROM Message m WHERE m.toUserId = ?1")
    List<Message> findMessagesToUserId(Long toUserId);

    @Query("SELECT m FROM Message m WHERE m.fromUserId = ?1 AND m.toUserId = ?2")
    List<Message> findMessagesFromUserIdToUserId(Long fromUserId, Long toUserId);

    @Query("SELECT m FROM Message m WHERE (m.timestamp >= ?1 and m.timestamp <= ?2)")
    List<Message> getAllMessages(Long timestampFrom, Long timestampTo);

    @Query("SELECT m FROM Message m WHERE (m.fromUserId = ?1 and m.timestamp >= ?2 and m.timestamp <= ?3)")
    List<Message> findMessagesFromUserId(Long fromUserId, Long timestampFrom, Long timestampTo);

    @Query("SELECT m FROM Message m WHERE (m.toUserId = ?1 and m.timestamp >= ?2 and m.timestamp <= ?3)")
    List<Message> findMessagesToUserId(Long toUserId, Long timestampFrom, Long timestampTo);

    @Query("SELECT m FROM Message m WHERE (m.fromUserId = ?1 AND m.toUserId = ?2 and m.timestamp >= ?3 and m.timestamp <= ?4)")
    List<Message> findMessagesFromUserIdToUserId(Long fromUserId, Long toUserId, Long timestampFrom, Long timestampTo);

    @Query("SELECT m FROM Message m WHERE ((m.fromUserId = ?1 OR m.toUserId = ?1) and m.timestamp >= ?2 and m.timestamp <= ?3)")
    List<Message> findMessagesUserId(Long userId, Long timestampFrom, Long timestampTo);
}
